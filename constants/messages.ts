export enum MessageType {
    Customer = 'Customer message',
    AngiX = 'AngiX Message',
    PII = 'PII Message', 
    QA = 'QA Message',
}

export type Message = {
    message: string,
    messageType: MessageType,
    options?: string[],
}

export enum ResponseType {
    landing = 'Landing',
    taskDetectionText = 'taskDetectionText',
    taskDetectionTextEnd = 'taskDetectionTextEnd',
    taskDetectionTemplateEnd = 'taskDetectionTemplateEnd',
    taskQaText = 'taskQaText',
    taskQaTemplate = 'taskQaTemplate',
    taskQaEnd = 'taskQaEnd',
    taskPIIText = 'taskPIIText',
    taskPIITemplate = 'taskPIITemplate',
    resultsMatched = 'ResultsMatched',
    resultsUnmatched = 'ResultsUnMatched',
}