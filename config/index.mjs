const isMockServerActive = process.env.MOCK === 'true';

export default {
    isMockServerActive,
  };