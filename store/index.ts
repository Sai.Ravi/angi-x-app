// TODO: Unit test
import type { Action, AnyAction, ThunkAction } from '@reduxjs/toolkit';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query/react';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import * as services from '../services';

const combinedReducer = combineReducers({
  ...services.reducers,
});

export type State = ReturnType<typeof combinedReducer>;

const reducer = (state: State | undefined, action: AnyAction): State => {
  return action.type === HYDRATE // Do server-side hydration
    ? {
        ...state, // use previous state
        ...action.payload, // apply delta from hydration
      }
    : combinedReducer(state, action);
};

export const makeStore = () => {
  const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(services.middlwares),
    devTools: process.env.NODE_ENV === 'development',
  });

  setupListeners(store.dispatch);
  return store;
};

type Store = ReturnType<typeof makeStore>;

export type AppDispatch = Store['dispatch'];
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  State,
  unknown,
  Action<string>
>;

export const wrapper = createWrapper(makeStore);
