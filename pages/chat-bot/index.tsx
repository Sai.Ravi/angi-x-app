import ChatBotModule from "../../components/organisms/ChatBotModule";
import Header from "../../components/organisms/Header";
import '../../src/app/globals.css';
import '../../src/app/fonts.scss';
import getConfig from "next/config";
import makeServer from '../../mocks/mirage';
import { wrapper } from '../../store';

const { isMockServerActive } =
  getConfig().publicRuntimeConfig;

if (isMockServerActive) makeServer();

function ChatBot() {
  console.log(isMockServerActive)
  return (
    <main className="theme-angi h-full bg-green-100 min-h-screen flex-col">
      <Header />
      <ChatBotModule />
    </main>
  )
}

export default wrapper.withRedux(ChatBot);
