const dynamicBrandingPreset =
  // eslint-disable-next-line import/no-extraneous-dependencies
  require('@angi/tailwindcss/presets/dynamic-branding').default;

module.exports = {
  // prefix: '@',
  important: false,
  // separator: ':',
  presets: [dynamicBrandingPreset],
  content: ['./components/**/*.{jsx,tsx}', './pages/**/*.{jsx,tsx}'],
};
