const tailwindConfig = require('./tailwind.config');

module.exports = {
  plugins: {
    'postcss-import': {},
    'tailwindcss/nesting': {},
    tailwindcss: tailwindConfig,
    autoprefixer: {},
  },
};