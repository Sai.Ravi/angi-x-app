import { ResponseType } from "../../constants/messages"

export const sessionData = {
    sessionId: '123',
    promptId: '456',
}

export const promptData = {
    promptId: '678',
}

export const botResponse = [
    {
        text: `Hello! I'm Angi Home Connect, your home services concierge. How can I assist you today with your home services needs?`,
        textType: ResponseType.landing,
    },
]

export const promptData1 = {
    promptId: '100',
}

export const botResponse1 = [
    {
        text: `Certainly! I'd be happy to help you with your lawn cleanup. To better assist you, could you please provide me with some additional details about the specific tasks you'd like to be done for your lawn cleanup? For example, do you need mowing, weed removal, leaf raking, or any other specific services?`,
        textType: ResponseType.taskDetectionText,
    },
]

export const promptData2 = {
    promptId: '200',
}

export const botResponse2 = [
    {
        text: `Thank you for clarifying that you need weed removal for your lawn. Could you please let me know the approximate size of your lawn? This will help me provide you with more accurate information and connect you with the right professionals for the task.`,
        options: ['Small: less than 2,500', 'Medium: 2,500 - 5,000', 'Large 5,000 - 10,000 (1/4 Acre)',
         'Very large: 10,000 - 20,000 (1/2 Acre)','Huge: more than 20,000 (1/2 Acre +)', 'Not sure'],
        textType: ResponseType.taskQaTemplate,
    },
]

export const promptDataAdd = {
    promptId: '400',
}

export const botResponseAdd = [
    {
        text: `Thanks. Are you planning/budgeting or ready to hire at this time?`,
        textType: ResponseType.taskQaText,
    },
]

export const promptData3 = {
    promptId: '300',
}

export const botResponse3 = [
    {
        text: 'Understood! What is your timeline to get this project done?',
        textType: ResponseType.taskQaText
    }
]

export const promptData4 = {
    promptId: '500',
}

export const botResponse4 = [
    {
        text: 'Thank you for providing the timeframe. Completing the lawn cleanup within two weeks is certainly achievable. To proceed further, we need the following information:',
        textType: ResponseType.taskQaText
    }
]