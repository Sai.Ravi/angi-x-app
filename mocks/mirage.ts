import { createServer } from 'miragejs';
import  {getSessionInfoRoute1, sendCustomerMessageRoute1, getBotResponseRoute1, 
  sendCustomerMessageRoute2, getBotResponseRoute2, sendCustomerMessageRoute3, getBotResponseRoute3, getBotResponseRoute4, sendCustomerMessageRoute4, sendCustomerMessageRouteAdd, getBotResponseRouteAdd, sendCustomerMessageRoute5, getBotResponseRoute5} from './routes/session';

export const makeServer = ({ environment = 'development' } = {}) =>
  createServer({
    environment,

    routes() {
      // this would allow the routes which are not mocked to pass through
      this.passthrough();
      getSessionInfoRoute1(this);
      sendCustomerMessageRoute1(this);
      getBotResponseRoute1(this);
      sendCustomerMessageRoute2(this);
      getBotResponseRoute2(this);
      sendCustomerMessageRoute3(this);
      getBotResponseRoute3(this);
      sendCustomerMessageRoute4(this);
      getBotResponseRoute4(this);
      sendCustomerMessageRouteAdd(this);
      getBotResponseRouteAdd(this);
      sendCustomerMessageRoute5(this);
      getBotResponseRoute5(this);
    },
  });

export default makeServer;
