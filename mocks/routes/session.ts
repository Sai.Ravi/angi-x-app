import type { Server } from 'miragejs';
import { botResponse, promptData, sessionData, botResponse1, promptData1, botResponse2, promptData2, promptData3, botResponse3, promptDataAdd, botResponseAdd, promptData4, botResponse4 } from '../mock-data/session';

export const getSessionInfoRoute1 = (server: Server) => {
  server.get(
    `/api/session`,
    () => {
      return sessionData;
    },
    { timing: 500}
  );
};

export const sendCustomerMessageRoute1 = (server: Server) => {
  server.post(
    `/api/session/123/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "Test";
      return promptData;
    },
    { timing: 500}
  );
};

export const getBotResponseRoute1 = (server: Server) => {
  server.get(
    `/api/session/123/prompt/456`,
    () => {
      return botResponse;
    },
    { timing: 1000}
  );
};

export const sendCustomerMessageRoute2 = (server: Server) => {
  server.post(
    `/api/session/123/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "There is a water leakage in my bedroom ceiling.";
      return promptData1;
    },
    { timing: 500}
  );
};

export const getBotResponseRoute2 = (server: Server) => {
  server.get(
    `/api/session/123/prompt/100`,
    () => {
      return botResponse1;
    },
    { timing: 1000}
  );
};

export const sendCustomerMessageRoute3 = (server: Server) => {
  server.post(
    `/api/session/456/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "There is a restroom on the top level.";
      return promptData2;
    },
    { timing: 500}
  );
};

export const getBotResponseRoute3 = (server: Server) => {
  server.get(
    `/api/session/456/prompt/200`,
    () => {
      return botResponse2;
    },
    { timing: 1000}
  );
};

export const sendCustomerMessageRoute4 = (server: Server) => {
  server.post(
    `/api/session/789/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "Bathtub";
      return promptData3;
    },
    { timing: 500}
  );
};

export const getBotResponseRoute4 = (server: Server) => {
  server.get(
    `/api/session/789/prompt/300`,
    () => {
      return botResponse3;
    },
    { timing: 1000}
  );
};

export const sendCustomerMessageRouteAdd = (server: Server) => {
  server.post(
    `/api/session/10/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "Bathtub";
      return promptDataAdd;
    },
    { timing: 500}
  );
};

export const getBotResponseRouteAdd = (server: Server) => {
  server.get(
    `/api/session/10/prompt/400`,
    () => {
      return botResponseAdd;
    },
    { timing: 1000}
  );
};

export const sendCustomerMessageRoute5 = (server: Server) => {
  server.post(
    `/api/session/11/prompt`,
    (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      attrs.customerMessage = "Bathtub";
      return promptData4;
    },
    { timing: 500}
  );
};

export const getBotResponseRoute5 = (server: Server) => {
  server.get(
    `/api/session/11/prompt/500`,
    () => {
      return botResponse4;
    },
    { timing: 1000}
  );
};
