// const withPlugins = require("next-compose-plugins");
// const withTM = require("next-transpile-modules")([
//     "@angi/icons",
//     "@angi/text-area",
//     "@angi/button",
//     "@angi/input-field", 
//     "@angi/radio", 
//   ]);
//   export const publicRuntimeConfig = {
//     isMockServerActive: config.isMockServerActive,
//   };
//   module.exports = withPlugins(
//     [
//       withTM,
//     ],
//   );


/** @type {import('next').NextConfig} */
import path from 'path';
import fs, { readFileSync } from 'fs';
import { fileURLToPath } from 'url';
import config from './config/index.mjs';

const buildCaseInsensitiveMatcher = (val) =>
  val
    .split('')
    .map((char) => [char.toLowerCase(), char.toUpperCase()])
    .map(([low, up]) => `(${low}|${up})`)
    .join('');

const brandHeaderMatchers = [
  buildCaseInsensitiveMatcher('angi'),
  buildCaseInsensitiveMatcher('homeadvisor'),
];

const brandsHeaderMatcher = `(${brandHeaderMatchers.join(')|(')})`;

const rewrites = () => [
  // Expose assets on a prefixed path so that we can set up a simple asset proxy
  // endpoint in root, if we run into CORS issues
  // { source: `${assetPrefix}/:path*`, destination: '/:path*' },
  { source: '/sm/debug/health', destination: '/api/health' },
  // simplify consumption by populating final path segment for pages that use
  // dynamic catch-all routes to support POST requests
  ...[
    '/customer-home/:brand/authenticated',
    '/customer-home/:brand/unauthenticated',
  ].map((source) => ({
    source,
    destination: `${source}/page`,
  })),
  {
    // move branding from HTTP header to path
    has: [
      {
        type: 'header',
        key: 'site-brand-key',
        value: `(?<brandHeader>${brandsHeaderMatcher})`,
      },
    ],
    source: '/customer-home/:path*',
    destination: '/customer-home/:brandHeader/:path*/page',
  },
];

const dirname = path.dirname(fileURLToPath(import.meta.url));
const nodeModules = path.resolve(dirname, 'node_modules');

const transpileScopes = ['@angi'];

const transpilePackages = fs
  .readdirSync(nodeModules)
  .filter((scope) => transpileScopes.includes(scope))
  .map((scope) =>
    fs
      .readdirSync(path.resolve(nodeModules, scope))
      .filter((dirName) => !dirName.startsWith('.'))
      .map((dirName) =>
        path.resolve(nodeModules, scope, dirName, 'package.json')
      )
  )
  .reduce((allPaths, scopePaths) => [...allPaths, ...scopePaths])
  .map((pathname) => readFileSync(pathname))
  .map(JSON.parse)
  .filter(
    ({ ignoreTranspileModules, name }) =>
      !ignoreTranspileModules &&
      name !== '@angi/next-app-platform' &&
      name !== '@angi/react-utils'
  )
  .map(({ name }) => name);

const publicRuntimeConfig = {
  env: config.env,
  isMockServerActive: config.isMockServerActive,
};

/** @type {import('next').NextConfig} */
export default {
  assetPrefix: config.assetPrefix,
  publicRuntimeConfig,
  rewrites,
  transpilePackages,
};
