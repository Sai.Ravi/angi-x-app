The structure of this directory parallels the locations of the files on HA and the HA CDN, which simplifies font face definitions that support accessing the application through `root` on the HA domain, through `root` on the Angi domain (`my.angi.com`), or directly. Prioritization is as follows:

1. HA CDN
2. HA (non-CDN; this is needed for `my.angi.com` because the HA CDN does not support CORS to that domain)
3. Application public assets
