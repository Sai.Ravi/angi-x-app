// TODO: unit test
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { HYDRATE } from 'next-redux-wrapper';
import { addCommonParams } from './helpers';

export const sessionApiV0 = createApi({
  reducerPath: 'sessionApiV0',
  baseQuery: fetchBaseQuery({ baseUrl: '/api' }),
  extractRehydrationInfo(action, { reducerPath }) {
    return action.type === HYDRATE ? action.payload[reducerPath] : undefined;
  },
  endpoints: (build) => ({
    getSessionInfo: build.query<
      ISessionDto,
      void
    >({
      query: () =>
        addCommonParams({
          url: `/session`,
          method: 'GET',
        }),
    }),
    sendCustomerMessage: build.query<
      IPromptResponseDto,
      { customerMessage: string, sessionId: string}
    >({
      query: ({customerMessage, sessionId}) =>
        addCommonParams({
          url: `/session/${sessionId}/prompt`,
          method: 'POST',
          body: { customerMessage }
        }),
    }),
    getBotResponse: build.query<
    IBotResponseDto,
    { sessionId: string, promptId: string}
  >({
    query: ({sessionId, promptId}) =>
      addCommonParams({
        url: `/session/${sessionId}/prompt/${promptId}`,
        method: 'GET'
      }),
  }),
  }),
});

export const { useGetSessionInfoQuery, useGetBotResponseQuery, useSendCustomerMessageQuery } = sessionApiV0;