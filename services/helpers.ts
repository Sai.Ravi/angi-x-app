// TODO: Unit test
import type { FetchArgs } from '@reduxjs/toolkit/query/react';
import getConfig from 'next/config';

const { apiHost, apiUser, env } = getConfig().publicRuntimeConfig;

// TODO: add the ability to set a user via headers when hitting the internal URL
// hasavingsmember2020@gmail.com
const defaultUser = {
  entityId: 107411645,
  entityHash: '107411645_d41d8cd98f00b204e9800998ecf8427e',
};

const addCommonParams = (query: string | FetchArgs) => {
  const isString = typeof query === 'string';

  const config: FetchArgs = {
    ...(isString ? {} : query),
    url: isString ? query : query.url,
  };

  config.params = {
    ...(config.params || {}),
    ...apiUser,
  };

  return config;
};

export const getBaseUrl = (path: string) =>
typeof window === 'undefined' ? `https://${apiHost}/${path}` : `${path}`;

// eslint-disable-next-line import/prefer-default-export
export { addCommonParams };
