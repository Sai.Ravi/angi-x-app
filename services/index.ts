import { sessionApiV0 } from './session-v0';

export const reducers = {
   [sessionApiV0.reducerPath]: sessionApiV0.reducer,
};

export const middlwares = [
    sessionApiV0
].map(({ middleware }) => middleware);

export {
  sessionApiV0,
};
