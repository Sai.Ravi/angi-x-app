interface ISessionDto {
    sessionId: string;
    authenticationInfo?: string;
    promptId: string;
}

interface IPromptResponseDto {
    promptId: string;
}

type IBotResponseDto = List<BotResponseText>;

interface BotResponseText {
    text: string;
    textType: ResponseType;
    options?: string[];
}


  