import React from 'react';
import {Message, MessageType} from '../../../constants/messages';
import MessageBubbleCustomer from '../../atoms/MessageBubbleCustomer';
import MessageBubbleAngiX from '../../atoms/MessageBubbleAngiX';
import PIIForm from '../PIIForm';
import QAForm from '../QAForm';

function ChatMessages({
  messages,
  onPIIFormSubmit,
  onQASubmit,
}: {
  messages: Message[];
  onPIIFormSubmit: (event: any) => void;
  onQASubmit: (submittedValue: any) => void;
}) {

  const onSubmit = (event: any) => {
    onPIIFormSubmit(event);
  };

  const handleQASubmit = (event: any) => {
    onQASubmit(event);
  }
  
  return (
    <div className='mx-20 overflow-y-scroll'>
      {messages.map((message) => {
        if (message.messageType === MessageType.AngiX) {
          return <MessageBubbleAngiX message={message.message} />;
        }
        if (message.messageType === MessageType.Customer) {
          return <MessageBubbleCustomer message={message.message} />;
        }
        if (message.messageType === MessageType.PII) {
          return (
            <PIIForm
              message={message.message}
              onPIIFormSubmit={(event) => onSubmit(event)}
            />
          );
        }
        if (message.messageType === MessageType.QA) {
            return (
              <QAForm
                message={message.message}
                onQASubmit={(submittedValue) => handleQASubmit(submittedValue)}
                options={message.options}
              />
            );
          }
      })}
    </div>
  );
}

export default ChatMessages;
