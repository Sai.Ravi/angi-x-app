import React, {Ref, RefObject, useRef, useState} from 'react';
import Radio from '@angi/radio';
import styles from './QAForm.module.css';
import Button from '@angi/button';
import AngiAvatarIcon from '../../atoms/AngiAvatarIcon';

function QAForm({
  message,
  onQASubmit,
  options
}: {
  message: string;
  onQASubmit: (submittedValue: any) => void;
  options?: string[];
}) {
  // const [selected, setSelected] = useState(false);
  const [selectedValue, setSelectedValue] = useState(null);
  // const buttonRef = useRef<HTMLButtonElement>(null);
  const formRef = useRef<HTMLFormElement>(null);

  const handleSelect = (event: any) => {
    // setSelected(true)
    setSelectedValue(event.target.name);
    onQASubmit(event.target.name);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    onQASubmit(selectedValue);
  };

  return (
    <div className='flex relative'>
      <AngiAvatarIcon className='float-down absolute bottom-4' />
      <form
        className={`${styles.root} my-4 md:min-w-[352px] md:max-w-[60%] flex p-4 rounded-t-[12px]
    rounded-br-[12px] text-gray-500 bg-gray-100 border ml-8`}
        onSubmit={(event) => handleSubmit(event)}
        ref={formRef}
      >
        <ul>
          <li className='mb-6'>
            <p className='inline mb-4'>{message}</p>
          </li>
          {options?.map((option) => (
            <li className='mb-6'>
              <Radio
                label={option}
                type='radio'
                name={option}
                onChange={(event) => {
                  handleSelect(event);
                }}
                disabled={!!selectedValue}
              />
            </li>
          ))}
          {/* <li className='mb-6'>
            <Radio
              type='radio'
              label='Something else'
              name='Something else'
              onChange={(event) => {
                handleSelect(event);
              }}
              disabled={!!selectedValue}
            />
          </li> */}
        </ul>
      </form>
    </div>
  );
}

export default QAForm;
