import React, {useState} from 'react';
import InputField from '@angi/input-field';
import Button from '@angi/button';
import AngiAvatarIcon from '../../atoms/AngiAvatarIcon';

function PIIForm({
  message,
  onPIIFormSubmit
}: {
  message: string;
  onPIIFormSubmit: (event: any) => void;
}) {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [zip, setZip] = useState('');
  const [submitButtonEnabled, setSubmitButtonEnabled] = useState(true);
   
  const onSubmit = (event: any) => {
    event.preventDefault();
    onPIIFormSubmit(event);
    setSubmitButtonEnabled(false);
  };

  return (
    <div className='flex relative'>
      <AngiAvatarIcon className='float-down absolute bottom-4' />
      <form
        className='my-4 md:min-w-[352px] md:max-w-[50%] flex p-4 rounded-t-[12px]
    rounded-br-[12px] text-gray-500 bg-gray-100 border ml-8'
        onSubmit={(event: any) => onSubmit(event)}
      >
        <ul>
          <li className='mb-4'>
            <p className='inline mb-4'>{message}</p>
          </li>
          <li className='mb-4'>
            <p className='inline mb-4'>*Angi will only share your personal with the pros you choose</p>
          </li>
          <li className='mb-4'>
            <InputField label='Name' value={name} onChange={(e) => {setName(e.target.value)}} className='bg-white' required />
          </li>
          <li className='mb-4'>
            <InputField label='Phone' value={phone} onChange={(e) => {setPhone(e.target.value)}} className='bg-white' required />
          </li>
          <li className='mb-4'>
            <InputField label='Email' value={email} onChange={(e) => {setEmail(e.target.value)}} className='bg-white' required />
          </li>
          <li className='mb-4'>
            <InputField label='ZIP Code' value={zip} onChange={(e) => {setZip(e.target.value)}} className='bg-white' required />
          </li>
          <li className='mb-4'>
            <Button type='submit' disabled={!name.length || !phone.length || !email.length || !zip.length || !submitButtonEnabled} fullBleed>
              Submit
            </Button>
          </li>
        </ul>
      </form>
    </div>
  );
}

export default PIIForm;
