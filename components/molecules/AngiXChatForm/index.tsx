import React, { RefObject, useState } from 'react';
import { AutogrowTextArea } from '@angi/text-area';
import Button from '@angi/button';
import ChatIcon from '../../atoms/ChatIcon';

function AngiXChatForm({onChatSubmit, isCustomerResponseEnabled, placeholderContent}: {onChatSubmit: (payload: any) => void, isCustomerResponseEnabled: boolean, placeholderContent: string}) {
  const [messageEntered, setMessageEntered] = useState('');
  const onSubmit = (event: any, text: any) => {
    event.preventDefault();
    onChatSubmit(text);
    setMessageEntered('');
  };
  return (
    <form onSubmit={(e) => onSubmit(e, messageEntered)} className=" overflow-hidden flex justify-center absolute bottom-0 md:w-full w-[80%]">
      <AutogrowTextArea required value={messageEntered} onChange={(event) => setMessageEntered(event.target.value)} placeholder={placeholderContent} className="bg-white rounded"></AutogrowTextArea>
      {/* <Button disabled={!isCustomerResponseEnabled} className="inline-block" type="submit">Enter</Button> */}
      <button className="absolute inline-block right-2 top-3" type="submit"><ChatIcon isActive={isCustomerResponseEnabled && messageEntered.length >0}/></button>
    </form>
  );
}

export default AngiXChatForm;
