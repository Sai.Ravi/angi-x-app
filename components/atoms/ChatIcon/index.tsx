import React from 'react';

const ChatIcon = ({isActive}: {isActive: boolean}) => (
  <svg
    width='32'
    height='32'
    viewBox='0 0 32 32'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
  >
    <circle cx='16' cy='16' r='16' fill={isActive ? '#FC5647' : '#BCB9B4'} />
    <path
      fill-fillRule='evenodd'
      clipRule='evenodd'
      d='M18.8243 9.26285C18.4172 9.63604 18.3897 10.2686 18.7628 10.6757L22.7268 15H6.5C5.94772 15 5.5 15.4477 5.5 16C5.5 16.5523 5.94772 17 6.5 17H22.7268L18.7628 21.3243C18.3897 21.7314 18.4172 22.364 18.8243 22.7372C19.2314 23.1104 19.864 23.0828 20.2372 22.6757L25.7372 16.6757C26.0876 16.2934 26.0876 15.7066 25.7372 15.3243L20.2372 9.32428C19.864 8.91716 19.2314 8.88966 18.8243 9.26285Z'
      fill='white'
    />
  </svg>
);

export default ChatIcon;
