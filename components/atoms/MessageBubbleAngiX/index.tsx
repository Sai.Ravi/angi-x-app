import React from 'react';
import AngiAvatarIcon from '../AngiAvatarIcon';

function MessageBubbleAngiX({ message }: {message: string}) {
  return (
      <div className="flex relative">
        <AngiAvatarIcon className="float-down absolute bottom-4"/>
        <div className="inline-flex w-full my-4 md:min-w-[352px] md:max-w-[60%] h-min flex p-4 rounded-t-[12px]
        rounded-br-[12px] text-gray-500 bg-gray-100 border ml-8">
          <p>{ message }</p>
        </div>
      </div>
  );
}

export default MessageBubbleAngiX;