import React, { RefObject } from 'react';

function MessageBubbleCustomer({ message }: {message: string}) {
  return (
    <div className="inline-flex w-full justify-end">
      <div className="inline-flex my-4 md:min-w-[352px] md:max-w-[60%] h-min flex p-4 rounded-t-[12px]
      rounded-bl-[12px] text-white bg-green-500 border">
        <p>{ message }</p>
      </div>
    </div>
  );
}

export default MessageBubbleCustomer;