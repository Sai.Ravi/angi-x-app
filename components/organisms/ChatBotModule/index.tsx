/* eslint-disable import/no-extraneous-dependencies */
import React, {useEffect, useRef, useState} from 'react';
import AngiXChatForm from '../../molecules/AngiXChatForm';
import { MessageType } from '../../../constants/messages';
import ChatMessages from '../../molecules/ChatMessages';
import { useGetBotResponseQuery, useGetSessionInfoQuery, useSendCustomerMessageQuery } from '../../../services/session-v0';

function ChatBotModule() { 

  const initialMessages = [
    // {messageType: MessageType.AngiX, message: 'Angi Home Connect will be at your service shorly'}, 
    // {messageType: MessageType.Customer, message: 'Test message from Customer 1'},
    // {messageType: MessageType.AngiX, message: 'Test message from Bot 2'}, 
    // {messageType: MessageType.Customer, message: 'est message from Customer 2'},
    // {messageType: MessageType.Customer, message: 'est message from Customer 3'},
    // {messageType: MessageType.Customer, message: 'est message from Customer 4'},
    // {messageType: MessageType.PII, message: 'Category Placeholder'},
    // {messageType: MessageType.QA, message: 'Which category best fits your project?',
    //  options:['Bathrooms', 'Septic Tanks & Wells', 'Plumbing', 'Water Treatment']}
  ];

  const [messages, setMessages] = useState(initialMessages);
  const [customerMessageReceived, setCustomerMessageReceived] = useState(0);
  const [isCustomerResponseEnabled, setIsCustomerResponseEnabled] = useState(false);
  const [placeholderContent, setPlaceHolderContent] = useState('What do you need done?');

  const { data } = useGetSessionInfoQuery();
    const {sessionId, promptId} = data || {};
    console.log(sessionId);
    console.log(promptId);
    const botResponse = useGetBotResponseQuery({sessionId: sessionId || '', promptId: promptId || ''});
    console.log(botResponse);
    // setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
    //   message: botResponse.data?.[0]?.text }]);

    useEffect(() => {
      const botResponseText = botResponse?.data?.[0]?.text;

      if(botResponseText){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
          message: botResponseText }]);
      }
      setPlaceHolderContent('Send a message.');
      setIsCustomerResponseEnabled(true);
    }, [botResponse]);

    const sendCustomerMessageResponse = useSendCustomerMessageQuery({customerMessage: "Our lawn is a mess and need cleanup", sessionId: '123'}, {skip: !(customerMessageReceived === 1)})

    const botResponse2 = useGetBotResponseQuery({promptId: sendCustomerMessageResponse?.data?.promptId || '', sessionId: sessionId || ''});

    const sendCustomerMessageResponse2 = useSendCustomerMessageQuery({customerMessage: "weed removal", sessionId: '456'}, {skip: !(customerMessageReceived === 2)})

    const botResponseAdd = useGetBotResponseQuery({promptId: sendCustomerMessageResponse2?.data?.promptId || '', sessionId: '456' || ''});

    const sendCustomerMessageResponseAdd = useSendCustomerMessageQuery({customerMessage: "Medium: 2,500 - 5,000", sessionId: '10'}, {skip: !(customerMessageReceived === 3)})

    const botResponse3 = useGetBotResponseQuery({promptId: sendCustomerMessageResponseAdd?.data?.promptId || '', sessionId: '10' || ''});

    const sendCustomerMessageResponse3 = useSendCustomerMessageQuery({customerMessage: "Just budgeting, looking for quotes.", sessionId: '789'}, {skip: !(customerMessageReceived === 4)})

    const botResponse4 = useGetBotResponseQuery({promptId: sendCustomerMessageResponse3?.data?.promptId || '', sessionId: '789' || ''});

    const sendCustomerMessageResponse4 = useSendCustomerMessageQuery({customerMessage: "I want to finish this in 2 weeks", sessionId: '11'}, {skip: !(customerMessageReceived === 5)})

    const botResponse5 = useGetBotResponseQuery({promptId: sendCustomerMessageResponse4?.data?.promptId || '', sessionId: '11' || ''});

    useEffect(() => {
      const botResponseText = botResponse2?.data?.[0]?.text;

      if(botResponseText){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
          message: botResponseText }]);
      }
      setIsCustomerResponseEnabled(true);
    }, [botResponse2]);

    

    useEffect(() => {
      const botResponseText1 = botResponseAdd?.data?.[0]?.text;

      if(botResponseText1){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.QA,
          message: botResponseText1, options: botResponseAdd?.data?.[0]?.options }]);
      }
      setIsCustomerResponseEnabled(true);
    }, [botResponseAdd]);

    useEffect(() => {
      // const botResponseText1 = botResponse3?.data?.[0]?.text;
      const botResponseText2 = botResponse3?.data?.[0]?.text;

      // if(botResponseText1){
      //   setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
      //     message: botResponseText1 }]);
      // }
      if(botResponseText2){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
          message: botResponseText2}]);
      }
      setIsCustomerResponseEnabled(true);
    }, [botResponse3]);

    useEffect(() => {
      //  const botResponseText1 = botResponse4?.data?.[0]?.text;
      const botResponseText2 = botResponse4?.data?.[0]?.text;

      // if(botResponseText1){
      //   setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
      //     message: botResponseText1 }]);
      // }
      if(botResponseText2){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
          message: botResponseText2}]);
      }
      setIsCustomerResponseEnabled(true);
    }, [botResponse4]);

    useEffect(() => {
      //  const botResponseText1 = botResponse4?.data?.[0]?.text;
      const botResponseText2 = botResponse5?.data?.[0]?.text;

      // if(botResponseText1){
      //   setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
      //     message: botResponseText1 }]);
      // }
      if(botResponseText2){
        setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.PII,
          message: botResponseText2}]);
      }
      setIsCustomerResponseEnabled(true);
    }, [botResponse5]);
    

  const onChatSubmit= async (text: any) => {
    setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.Customer,
       message: text }]);

    setCustomerMessageReceived((old) => old+1);
    setIsCustomerResponseEnabled(false);
    await delay(1000);
    
    console.log(customerMessageReceived);
    
  }
  
  useEffect(() => {
    let elem = document.getElementById('scroller');
    elem && (elem.scrollTop = elem.scrollHeight);
  });

  const handlePIIFormSubmit = async (event: any) => {
    setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
      message: `Thank you for providing your contact info.  We have matched you with some top-rated pros at ${event.target[3].value}` }]);
    setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
        message: 'Thanks for using Angi Home Connect. You will be redirected to the results page shortly.' }]);
        await delay(5000);
        window.location.href="https://match.angi.com/myhomeadvisor/pro-results/269824783";
      
    console.log(event.target[0].value);
    console.log(event.target[1].value);
    console.log(event.target[2].value);
  }

  const delay = (ms: number | undefined) => new Promise(res => setTimeout(res, ms));

  const handleQASubmit = async (submittedValue: any) => {
    if(submittedValue=== 'Something else') {
      setMessages((oldmessages)=> [...oldmessages, {messageType: MessageType.AngiX,
        message: 'Sorry, I am having difficulty find the matching home service for your issue. Let me redirect you to angi.com' }]);
         await delay(5000);
         window.location.href="https://match.angi.com/sitesearch/searchQuery?action=SEARCH&startIndex&useExtSearch=false&searchType=SiteTaskSearch&initialSearch=true&sourcePage=Homepage&entry_point_id=33797112&query";
    }
    else {
      setCustomerMessageReceived((old) => old+1);
    }
    console.log(submittedValue);
  }

  return (
    <div className="md:px-[100px]">
      <div className="md:px-10 text-center mt-[50px]">
        <h1 className="mb-6">We’ll find top-rated certified pros in your area</h1>
      </div>
      <div>
      <div className="relative drop-shadow-lg">
        <div id="scroller" className="overflow-y-scroll justify-center
          bg-white md:h-[700px] my-12 pt-12 pb-20">
          <ChatMessages messages={messages} onPIIFormSubmit={(event) => handlePIIFormSubmit(event)}  onQASubmit={(submittedValue) => handleQASubmit(submittedValue)}/>
          <AngiXChatForm placeholderContent={placeholderContent} isCustomerResponseEnabled={isCustomerResponseEnabled} onChatSubmit={(text) => onChatSubmit(text)}/>
        </div>
      </div>
      </div>
    </div>
  );
}

export default ChatBotModule;
