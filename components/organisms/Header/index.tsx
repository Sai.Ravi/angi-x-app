/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import {AngiIcon} from '@angi/icons';
import styles from './Header.module.scss';

function Header() {
  return (
    <header>
      <div className={styles.content}>
        <a
          id='Link: Header Logo'
          href='https://angi.com/customer'
          aria-label='Home'
        >
          <AngiIcon className={styles.angiIcon} />
        </a>
        <h2 className="text-green-500 md:flex justify-end absolute nt:right-[156px] md:right-12 hidden">Angi Home Connect</h2>
      </div>
    </header>
  );
}

export default Header;
